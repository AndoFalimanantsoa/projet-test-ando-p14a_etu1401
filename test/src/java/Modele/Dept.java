/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import Annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author P14A_30_Ando
 */
public class Dept {
    int idDept;
    String nomDept;

    public Dept() {
    }

    public Dept(int idDept, String nomDept) {
        this.idDept = idDept;
        this.nomDept = nomDept;
    }

    public int getIdDept() {
        return idDept;
    }

    public void setIdDept(int idDept) {
        this.idDept = idDept;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }
    
    @Annotation(url="Dept-lister")
    public ModeleView lister(){
        ModeleView mv=new ModeleView();
        mv.setUrlDispatch("listeDept.jsp");
        ArrayList<Dept> listeDept=new ArrayList<Dept>();
        listeDept.add(new Dept(1,"Approvisionnement"));
        listeDept.add(new Dept(2,"Ressources humaines"));
        listeDept.add(new Dept(3,"Comptabilité et finance"));
        listeDept.add(new Dept(4,"IT"));
        listeDept.add(new Dept(1,"Communication"));
        HashMap<String , Object>valiny = new HashMap<>();
        valiny.put("listeDept", listeDept);
        mv.setHmap(valiny);
        return mv;
    }

    @Annotation(url="Dept-ajouter")
    public ModeleView ajouter(){
        ModeleView mv=new ModeleView();
        mv.setUrlDispatch("AjoutDept.jsp");
        System.out.print(this.getNomDept());
        return mv;
    }
    
    

}
